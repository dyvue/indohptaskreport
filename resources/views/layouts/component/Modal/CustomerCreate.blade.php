  <div class="modal fade" id="customerCreateModal" tabindex="-1" role="dialog" aria-labelledby="customerCreateModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header bg-secondary">
                  <h5 class="modal-title text-white" id="customerCreateLabel">Create New Customer</h5>
                  <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form id="customerCreateForm" method="POST" action="{{ route('customers.store') }}">
                  @csrf
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="task">Task</label>
                          <input type="text" name="task" class="form-control" placeholder="Enter Task" required>
                      </div>
                      <div class="form-group">
                          <label for="card_id">Assignee</label>
                          <select name="user_id[]" class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Choose ...">
                              @foreach($users as $res)
                              <option value="{{ $res->id }}">{{ $res->name }}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <div class="row">
                              <div class="col-6">
                                  <label for="mitra">Paket Mitra</label>
                                  <select name="package" id="mitra" class="form-control" required>
                                      <option value="" selected>&mdash;</option>
                                      @php
                                      $mitras = [
                                      [
                                      "name" => "Universal",
                                      "price" => "50000",
                                      ],
                                      [
                                      "name" => "Branding",
                                      "price" => "599000",
                                      ],
                                      [
                                      "name" => "Professional",
                                      "price" => "3000000",
                                      ]
                                      ];
                                      @endphp
                                      @foreach($mitras as $mitra)
                                      <option value="{{ $mitra['name'] }}" price="{{ $mitra['price'] }}">{{ $mitra['name'] }}</option>
                                      @endforeach
                                  </select>
                              </div>
                              <div class="col-6">
                                  <label for="harga">Harga</label>
                                  <input type="number" name="price" id="price" class="form-control" placeholder="Harga" required>
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label>Tanggal Close</label>
                                      <input id="date-close" name="date_close" type="text" class="form-control" name="date_close" required>
                                      <span class="font-13 text-muted">e.g "MM/DD/YYYY"</span>
                                  </div>
                              </div>
                              <div class="col-6">
                                  <label for="priority">Priority</label>
                                  <select name="priority" id="priority" class="form-control" required>
                                      <option value="" selected>&mdash;</option>
                                      @php
                                      $priorities = ["Urgent", "High", "Normal", "Low", "Additional"];
                                      @endphp
                                      @foreach($priorities as $res)
                                      <option value="{{ $res }}">{{ $res }}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="form-group">
                              <label for="payment">Payment</label>
                              <select name="payment" class="form-control" placeholder="Payment" required>
                                  <optgroup label="Bank Transfer Manual"></optgroup>
                                  <option value="Bank BCA">Bank BCA</option>
                                  <option value="Bank BNI">Bank BNI</option>
                                  <option value="Bank MANDIRI">Bank MANDIRI</option>
                                  <option value="Bank BRI">Bank BRI</option>
                                  <option value="Bank BTN">Bank BTN</option>
                                  <option value="Bank BTPN">Bank BTPN</option>
                                  <option value="Bank CIMB">Bank CIMB</option>
                                  <option value="Bank BJB">Bank BJB</option>
                                  <option value="Bank DANAMON">Bank DANAMON</option>
                                  <option value="Bank DBS">Bank DBS</option>
                                  <option value="Bank Muamalat">Bank Muamalat</option>
                                  <option value="Bank Sinarmas">Bank Sinarmas</option>
                                  <option value="Bank Lainnya">Bank Lainnya</option>
                                  </optgroup>
                                  <optgroup label="Xendit"></optgroup>
                                  <option value="Xendit Alfamart">Alfamart</option>
                                  <option value="Xendit Virtual Account">Virtual Account</option>
                                  <option value="Xendit OVO">OVO</option>
                                  <optgroup label="Lainnya"></optgroup>
                                  <option value="Pulsa">Pulsa</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="form-group">
                              <label for="comment">Comment</label>
                              <textarea name="comment" id="comment" class="form-control"></textarea>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary rounded">Submit</button>
                  </div>
              </form>
          </div>
      </div>
  </div>

  <script>
      let selectMitra = document.getElementById('mitra')

      selectMitra.addEventListener('change', function() {
          let inputPrice = document.getElementById('price')
          inputPrice.value = selectMitra.options[this.selectedIndex].getAttribute('price')
      })
  </script>