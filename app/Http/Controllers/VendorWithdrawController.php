<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\VendorWithdraw;
use App\Model\VendorWithdrawReport;
Use App\User;

Use DB;

class VendorWithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $d['users'] = User::orderBy("name", "ASC")->get();
        $d['vendorwithdraws'] = VendorWithdraw::orderBy('id', "DESC")->get();
        return view('app.VendorWithdraw.index', $d);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c = VendorWithdraw::orderBy("id", "DESC")->first();
        $d = new VendorWithdraw;
        if(!empty($c)){
            $vendorWithdrawID = $c->id + 1;
        }
        else{
            $vendorWithdrawID = 1;
        }
        $d->id = $vendorWithdrawID;
        $d->task = $request->input('task');
        $d->nominal = $request->input('nominal');
        $d->payment = $request->input('payment');
        $d->date = $request->input('date');
        $d->comment = $request->input('comment');

        $d->save();

        $userID = $request->input('user_id');

        $whereArray = array('vendor_withdraw_id' => $vendorWithdrawID);
        $query = DB::Table('vendor_withdraw_reports');
        foreach($whereArray as $field => $value) {
            $query->where($field, $value);
        }
        $query->delete();

        if(!empty($userID)){
            foreach($userID as $res){
                $e = new VendorWithdrawReport;
                $e->user_id = $res;
                $e->vendor_withdraw_id = $vendorWithdrawID;
    
                $e->save();
            }
        }

        return back()->with("alertStore", $request->input('task'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\VendorWithdraw  $vendorWithdraw
     * @return \Illuminate\Http\Response
     */
    public function show(VendorWithdraw $vendorWithdraw)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\VendorWithdraw  $vendorWithdraw
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorWithdraw $vendorWithdraw)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\VendorWithdraw  $vendorWithdraw
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorWithdraw $vendorWithdraw)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\VendorWithdraw  $vendorWithdraw
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorWithdraw $vendorWithdraw, $id)
    {
        $d = VendorWithdraw::findOrFail($id);
        $task = $d->task;
        $d->delete();

        return back()->with("alertDestroy", $task);
    }
}
