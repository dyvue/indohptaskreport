<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendorWithdraw extends Model
{
    public function vendorWithdrawReports()
    {
        return $this->hasMany(VendorWithdrawReport::class);
    }
}
